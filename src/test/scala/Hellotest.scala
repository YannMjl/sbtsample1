import org.scalatest.FunSuite

class Hellotest extends FunSuite {
  test("say hello method works great!"){
    val  hello = new  Hello
    assert(hello.sayHello("Scala") == "Hello, Scala!")
  }
}


