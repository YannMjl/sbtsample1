publishMavenStyle := true

name := "SbtExample"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.4"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.4" % "test"

publishTo := Some("sbtrepo" at "http://examples-yann.mycloudrepo.localhost:6123/repositories/sbtrepo")

credentials += Credentials("sbtrepo", "http://examples-yann.mycloudrepo.localhost:6123/repositories/sbtrepo", "user1@example.com", "123user1")
